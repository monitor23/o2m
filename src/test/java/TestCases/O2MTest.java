package TestCases;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import ObjectRepositories.O2MLogin;
import ObjectRepositories.O2MHome;

public class O2MTest {
	WebDriver driver;

	@BeforeTest
	public void Initialization() {
		System.setProperty("webdriver.gecko.driver", "C:\\MyWorkspace\\geckodriver.exe");
		System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE, "true");
		System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE, "C:\\SeleniumLogs\\seleniumlogs.txt");
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("https://netlink04.netlink-testlabs.com/swp/customgroup/o2m/");

	}

	@Test
	public void O2MLogin() throws InterruptedException {
		O2MLogin ol = new O2MLogin(driver);
		ol.WaitFunction();
		ol.Username().sendKeys("netlink05");
		ol.Password().sendKeys("July2021July2021+");
		ol.Login().click();
		O2MHome oh = new O2MHome(driver);
		oh.GetTitle();

	}

}
