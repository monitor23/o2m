package ObjectRepositories;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class O2MHome {
	WebDriver driver;
	WebDriverWait wait;

	@SuppressWarnings("deprecation")
	public O2MHome(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver, 300);
		PageFactory.initElements(driver, this);
	}

	public void GetTitle() throws InterruptedException {

		Thread.sleep(8000);
		ArrayList<Object> tabs2 = new ArrayList<Object>(driver.getWindowHandles());
		// System.out.println(tabs2.size());
		driver.switchTo().window((String) tabs2.get(1));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("pageTitle")));
		String title = driver.getTitle();
		// System.out.println(title);

		if (title.equals("SWIFTNet Online Operations Manager")) {
			System.out.println("Login to SWIFTNet Online Operations Manager is successful");
			System.out.println("Title of the Page : " + title);
			String uName = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".loginInfoLabel")))
					.getText();
			System.out.println("Username : " + uName);
		} else {
			System.out.println("Login to SWIFTNet Online Operations Manager is failed");
			System.out.println("Title of the Page : " + title);
		}
		this.Logout();

	}

	public void Logout() throws InterruptedException {
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".logoutHoverLink"))).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(
				".dialogBoxLayout > tbody:nth-child(2) > tr:nth-child(2) > td:nth-child(2) > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(1) > td:nth-child(1) > button:nth-child(1)")))
				.click();
		Thread.sleep(2000);
		driver.close();
		Thread.sleep(2000);
		ArrayList<Object> tabs2 = new ArrayList<Object>(driver.getWindowHandles());
		// System.out.println(tabs2.size());
		driver.switchTo().window((String) tabs2.get(0));
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("gwt-debug-desktop-topmenu-Logout"))).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("gwt-debug-dialog-ask-0-ok"))).click();
		Thread.sleep(2000);
		driver.close();
	}

}
