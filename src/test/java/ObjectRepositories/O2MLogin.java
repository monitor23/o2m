package ObjectRepositories;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class O2MLogin {
	WebDriver driver;
	WebDriverWait wait;

	@SuppressWarnings("deprecation")
	public O2MLogin(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver, 300);
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "gwt-debug-platform_login-username")
	WebElement userName;
	@FindBy(id = "gwt-debug-platform_login-password")
	WebElement password;
	@FindBy(xpath = "//*[@id='gwt-debug-platform_login-logon']")
	WebElement login;

	public void WaitFunction() {

		wait.until(ExpectedConditions.visibilityOf(userName));

	}

	public WebElement Username() {
		return userName;
	}

	public WebElement Password() {
		return password;
	}

	public WebElement Login() {
		return login;
	}

}
